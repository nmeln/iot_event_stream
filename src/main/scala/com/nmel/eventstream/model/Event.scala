package com.nmel.eventstream.model


sealed trait Event {
  type T
  val value : T
  val time: Double
}
trait BooleanEvent extends Event {
  type T = Boolean
}
trait StatisticalEvent extends Event {
  type T = Long
}

case class TemperatureEvent(value: Long, time: Double) extends StatisticalEvent
case class HumidityEvent(value: Long, time: Double) extends StatisticalEvent
case class LightEvent(value: Boolean, time: Double) extends BooleanEvent

