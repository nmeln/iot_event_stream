package com.nmel.eventstream.model

import io.circe.Json

case class Stats(average: Double)
case class OutputEvent(event: Event, stats: Option[Stats] = None) {
  def toJson: Json = {
    import io.circe.syntax._
    import io.circe.generic.auto._
    event match {
      case t: TemperatureEvent => t.asJson
      case h: HumidityEvent    => h.asJson
      case l: LightEvent       => l.asJson
      case _ => Unit.asJson
    }
  }
}
