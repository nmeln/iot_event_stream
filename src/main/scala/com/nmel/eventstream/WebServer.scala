package com.nmel.eventstream

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.AmqpConnectionSettings
import com.nmel.eventstream.processor.{AmqpProcessor, Processor}
import com.nmel.eventstream.routes.WebsocketRoute
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import org.slf4j.LoggerFactory

trait Routes extends FailFastCirceSupport with WebsocketRoute

object WebServer extends Routes {
  import akka.http.scaladsl.server.Directives._
  val log = LoggerFactory.getLogger(WebServer.getClass)
  val serverPort = 9200
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  implicit val amqpSettings: AmqpConnectionSettings = ??? //TODO: Use actual settings
  implicit val processor: Processor = new AmqpProcessor(settings = amqpSettings)

  def main(args: Array[String]): Unit = {
    val routes = tempEvents ~ humidityEvents ~ lightEvents
    Http().bindAndHandleAsync(
      Route.asyncHandler(routes),
      "0.0.0.0",
      serverPort
    )
  }
}
