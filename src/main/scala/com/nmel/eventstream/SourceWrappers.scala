package com.nmel.eventstream

sealed trait QueueName
case class TemperatureQueueName(name: String) extends QueueName
case class HumidityQueueName(name: String) extends QueueName
case class LightQueueName(name: String) extends QueueName

