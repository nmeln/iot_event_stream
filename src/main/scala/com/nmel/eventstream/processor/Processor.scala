package com.nmel.eventstream.processor

import akka.NotUsed
import akka.stream.alpakka.amqp.{
  AmqpConnectionSettings,
  NamedQueueSourceSettings
}
import akka.stream.alpakka.amqp.scaladsl.AmqpSource
import akka.stream.scaladsl.Source
import com.nmel.eventstream.model._
import com.nmel.eventstream.{
  HumidityQueueName,
  LightQueueName,
  TemperatureQueueName
}

abstract class Processor {
  def sourceFromQueue(queueName: TemperatureQueueName): Source[TemperatureEvent, NotUsed]
  def sourceFromQueue(queueName: HumidityQueueName): Source[HumidityEvent, NotUsed]
  def sourceFromQueue(queueName: LightQueueName): Source[LightEvent, NotUsed]
}

//object KafkaProcessor extends Processor {
//  def processQueue(queueName: TemperatureQueueName)(implicit settings: SourceSettings): Source[TemperatureEvent, NotUsed] = ???
//  def processQueue(queueName: HumidityQueueName)(implicit settings: SourceSettings): Source[HumidityEvent, NotUsed] = ???
//  def processQueue(queueName: LightQueueName)(implicit settings: SourceSettings): Source[LightEvent, NotUsed] = ???
//}

class AmqpProcessor(settings: AmqpConnectionSettings) extends Processor {
  def sourceFromQueue(
      queueName: TemperatureQueueName): Source[TemperatureEvent, NotUsed] = {

    val temperatureQueue = AmqpSource.atMostOnceSource(
      NamedQueueSourceSettings(settings, queueName.name),
      bufferSize = 10
    )
    temperatureQueue.map { msg =>
      val value :: time :: Nil = msg.bytes.utf8String.split(',').take(2).toList
      TemperatureEvent(value.toLong, time.toLong)
    }
  }

  def sourceFromQueue(
      queueName: HumidityQueueName): Source[HumidityEvent, NotUsed] = {

    val humidityQueue = AmqpSource.atMostOnceSource(
      NamedQueueSourceSettings(settings, queueName.name),
      bufferSize = 10
    )
    humidityQueue.map { msg =>
      val value :: time :: Nil = msg.bytes.utf8String.split(',').take(2).toList
      HumidityEvent(value.toLong, time.toLong)
    }
  }

  def sourceFromQueue(queueName: LightQueueName): Source[LightEvent, NotUsed] = {

    val lightQueue = AmqpSource.atMostOnceSource(
      NamedQueueSourceSettings(settings, queueName.name),
      bufferSize = 10
    )
    lightQueue.map { msg =>
      val value :: time :: Nil = msg.bytes.utf8String.split(',').take(2).toList
      LightEvent(value.toBoolean, time.toLong)
    }
  }
}
