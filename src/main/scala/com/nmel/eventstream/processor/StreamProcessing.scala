package com.nmel.eventstream.processor

import akka.NotUsed
import akka.stream.scaladsl._
import com.nmel.eventstream.model._
import com.nmel.eventstream.{HumidityQueueName, LightQueueName, QueueName, TemperatureQueueName}

trait StreamProcessing {
  def streamFromQueue(queueName: QueueName)(implicit processor: Processor)
    : Source[OutputEvent, NotUsed] = {

    import processor._

    val eventQueue: Source[OutputEvent, NotUsed] = queueName match {
        case t: TemperatureQueueName =>
          val source = sourceFromQueue(t)
          calculateAverages(source)
        case h: HumidityQueueName    =>
          val source = sourceFromQueue(h)
          calculateAverages(source)
        case l: LightQueueName       =>
          val source = sourceFromQueue(l)
          source.map(OutputEvent(_, None))
    }
    eventQueue
  }

  def calculateAverages(source: Source[Event, NotUsed]): Source[OutputEvent, NotUsed] = {
    source.statefulMapConcat { () =>
      var idx = 0L
      var avg = 0.0

      (event: Event) =>
        {
          event match {
            case s: StatisticalEvent =>
              idx += 1
              avg += s.value / idx
              List(OutputEvent(event, Option(Stats(average = avg))))

            case _: BooleanEvent =>
              List(OutputEvent(event, None))
          }
        }
    }
  }

}
