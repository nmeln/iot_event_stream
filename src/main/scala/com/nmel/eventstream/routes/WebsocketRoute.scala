package com.nmel.eventstream.routes

import akka.http.scaladsl.model.ws.TextMessage
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import com.nmel.eventstream.processor.{Processor, StreamProcessing}
import com.nmel.eventstream.{HumidityQueueName, LightQueueName, TemperatureQueueName}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.AutoDerivation

trait WebsocketRoute extends StreamProcessing with AutoDerivation {
  self: FailFastCirceSupport  =>
  implicit val materializer: ActorMaterializer
  implicit val processor: Processor

  val temperatureQueue = TemperatureQueueName("temperature")
  val humidityQueue = HumidityQueueName("humidity")
  val lightQueue = LightQueueName("light")

  //streaming to all clients through websocket
  lazy val tempEvents: Route =
    path("temperature") {
      extractUpgradeToWebSocket { upgrade =>
        val outputSource = streamFromQueue(temperatureQueue).map { outputEvent =>
          TextMessage.Strict(outputEvent.toJson.spaces2) //TODO: replace with noSpaces in prod ;)
        }
        complete(
          upgrade.handleMessagesWithSinkSource(Sink.ignore, outputSource))
      }
    }

  lazy val humidityEvents: Route =
    path("humidity") {
      extractUpgradeToWebSocket { upgrade =>
        val outputSource = streamFromQueue(humidityQueue).map { outputEvent =>
          TextMessage.Strict(outputEvent.toJson.spaces2)
        }
        complete(
          upgrade.handleMessagesWithSinkSource(Sink.ignore, outputSource))
      }
    }

  lazy val lightEvents: Route =
    path("light") {
      extractUpgradeToWebSocket { upgrade =>
        val outputSource = streamFromQueue(lightQueue).map { outputEvent =>
          TextMessage.Strict(outputEvent.toJson.spaces2)
        }
        complete(
          upgrade.handleMessagesWithSinkSource(Sink.ignore, outputSource))
      }
    }
}
