name := "event_stream"

version := "0.1"

scalaVersion := "2.12.4"

val circeVersion = "0.8.0"

resolvers += Resolver.bintrayRepo("hseeberger", "maven")

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.4",
  "com.typesafe.akka" %% "akka-contrib" % "2.5.4",
  "com.typesafe.akka" %% "akka-distributed-data" % "2.5.4",
  "com.typesafe.akka" %% "akka-http" % "10.0.9",
  "de.heikoseeberger" %% "akka-http-circe" % "1.18.0",
  "com.lightbend.akka" %% "akka-stream-alpakka-amqp" % "0.14",
  "com.typesafe.akka" %% "akka-stream" % "2.5.4", // or whatever the latest version is,
  "com.typesafe.akka" %% "akka-testkit" % "2.5.4" % Test,
  "ch.qos.logback" % "logback-classic" % "1.2.3"
) ++ Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)
